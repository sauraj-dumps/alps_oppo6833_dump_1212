## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1622908711911 release-keys
- Manufacturer: alps
- Platform: mt6833
- Codename: oppo6833
- Brand: alps
- Flavor: sys_oplus_mssi_64_cn-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1622908711911
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: alps/vnd_oppo6833/oppo6833:11/RP1A.200720.011/1622908711911:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1622908711911-release-keys
- Repo: alps_oppo6833_dump_1212


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
